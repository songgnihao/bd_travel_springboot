package com.neutrino.mapper;

import com.neutrino.entity.QueryVo;
import com.neutrino.entity.TourismInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TourismMapper {
    List<TourismInfo> selectTourismInfoList();
    //总条数
    Integer postCountByQueryVo(QueryVo vo);
    //结果集
    List<TourismInfo> selectPostListByQueryVo(QueryVo vo);
    TourismInfo selectInfoByName(String name);
}
