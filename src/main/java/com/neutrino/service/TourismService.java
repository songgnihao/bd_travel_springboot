package com.neutrino.service;

import com.neutrino.entity.QueryVo;
import com.neutrino.entity.Scenic;
import com.neutrino.entity.TourismInfo;
import com.neutrino.utils.Page;

import java.util.List;

public interface TourismService {
    List<TourismInfo> selectTourismInfoList();
    Page<TourismInfo> selectPageByQueryVo(QueryVo vo);
    TourismInfo selectInfoByName(String name);
}
